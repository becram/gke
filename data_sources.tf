data "google_compute_zones" "available" {
  region = "${var.region}"
  status = "UP"
}

data "google_container_engine_versions" "versions" {
  location = "${data.google_compute_zones.available.names[0]}"
}
