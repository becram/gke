locals {
  # So we need to somehow ensure backward compatability with this module.  To
  # do this, we need to ensure the `google_container_cluster.location` object
  # has the correct information:
  #   * For a zonal cluster, location should be set to a single zone
  #   * For a regional cluster, location should be set to the region
  # Take all known zones as list, and transform it to a string separated by ","
  # Example:
  #   Input:
  #     [
  #       "us-east1-a",
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  #
  #  Becomes:
  #    "us-east1-a,us-east1-b,us-east1-c"
  string_all_node_locations = "${join(",", data.google_compute_zones.available.names)}"

  # Replace what we have historically used as the master zone, and replace it when an empty string
  # Example:
  #   Input:
  #    "us-east1-a,us-east1-b,us-east1-c"
  #  Becomes:
  #    ",us-east1-b,us-east1-c"
  no_primary_node_locations = "${replace(local.string_all_node_locations, data.google_compute_zones.available.names[0], "")}"

  # Set which string we want to us based on whether or not this is a regional or zonal cluster:
  #   * For a zonal cluster, it should resemble: ",us-east1-b,us-east1-c"
  #   * For a regional cluster, it should resemble: "us-east1-a,us-east1-b,us-east1-c"
  determine_node_locations = "${replace(replace(var.regional_cluster, "true", local.string_all_node_locations), "false", local.no_primary_node_locations)}"

  # Now take that string, turn it back into a list
  # Example:
  #   Input:
  #    ",us-east1-b,us-east1-c"
  #
  #  Becomes:
  #     [
  #       "",
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  clean_up_node_locations = "${split(",", local.determine_node_locations)}"

  # Remove any empty objects from that list
  # Example:
  #   Input:
  #     [
  #       "",
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  #
  #  Becomes:
  #     [
  #       "us-east1-b",
  #       "us-east1-c"
  #     ]
  finalize_node_locations = "${compact(local.clean_up_node_locations)}"
}

resource "google_container_cluster" "cluster" {
  name                     = "${format("%v-%v", var.environment, var.name)}"
  location                 = "${replace(replace(var.regional_cluster, "true", var.region), "false", data.google_compute_zones.available.names[0])}"
  node_locations           = ["${local.finalize_node_locations}"]
  initial_node_count       = "1"
  subnetwork               = "${google_compute_subnetwork.gke.name}"
  min_master_version       = "${var.kubernetes_version == "" ? data.google_container_engine_versions.versions.latest_master_version : var.kubernetes_version}"
  network                  = "${var.vpc}"
  project                  = "${var.project}"
  remove_default_node_pool = "true"

  addons_config {
    network_policy_config = {
      disabled = "${var.disable_network_policy}"
    }
  }

  network_policy {
    enabled  = "${var.node_network_policy}"
    provider = "${var.node_network_policy == "true" ? var.network_policy_provider : "PROVIDER_UNSPECIFIED" }"
  }

  ip_allocation_policy = {
    cluster_secondary_range_name  = "${google_compute_subnetwork.gke.secondary_ip_range.0.range_name}"
    services_secondary_range_name = "${google_compute_subnetwork.gke.secondary_ip_range.1.range_name}"
  }

  private_cluster_config {
    enable_private_nodes   = "${var.private_cluster}"
    master_ipv4_cidr_block = "${replace(replace(var.private_cluster, "true", var.private_master_cidr), "false", "")}"
  }

  master_authorized_networks_config {
    cidr_blocks = {
      # We'll need version 0.12 of terraform to make this more flexible
      # https://github.com/hashicorp/terraform/issues/7034
      cidr_block = "${var.authorized_master_access}"
    }
  }

  resource_labels {
    # Let's set a label on the cluster to let us know whether or not our
    # cluster is private or not.
    # This complex replace statement evaluates `var.private_cluster`.
    #   * If `true`, we'll replace the literal word true, with `private`
    #   * If `false, we'll replace the literal word false, with `public`
    # this is done this way cuz terraform is not a programming language
    # This trick can be found in more detail here:
    # https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9
    cluster_type = "${replace(replace(var.private_cluster, "true", "private"), "false", "public")}"

    cluster_loc = "${replace(replace(var.regional_cluster, "true", "regional"), "false", "zonal")}"
  }
}

resource "google_container_node_pool" "node_pool" {
  count    = "${length(var.node_pools)}"
  name     = "${lookup(var.node_pools[count.index], "name")}"
  project  = "${var.project}"
  location = "${replace(replace(var.regional_cluster, "true", var.region), "false", data.google_compute_zones.available.names[0])}"
  cluster  = "${google_container_cluster.cluster.name}"

  version = "${replace(replace(lookup(var.node_pools[count.index], "node_auto_upgrade"), "true", ""), "false", var.kubernetes_version)}"

  autoscaling = {
    min_node_count = 1
    max_node_count = "${lookup(var.node_pools[count.index], "max_node_count")}"
  }

  management = {
    auto_repair  = "${lookup(var.node_pools[count.index], "node_auto_repair")}"
    auto_upgrade = "${lookup(var.node_pools[count.index], "node_auto_upgrade")}"
  }

  node_config {
    disk_size_gb = "${lookup(var.node_pools[count.index], "node_disk_size_gb")}"
    disk_type    = "${lookup(var.node_pools[count.index], "node_disk_type")}"

    preemptible  = "${lookup(var.node_pools[count.index], "preemptible")}"
    machine_type = "${lookup(var.node_pools[count.index], "machine_type")}"

    oauth_scopes = "${var.oauth_scopes}"

    labels {
      environment = "${var.environment}"
      name        = "${var.name}"
      pool        = "${lookup(var.node_pools[count.index], "name")}"
      preemptible = "${lookup(var.node_pools[count.index], "preemptible")}"
    }

    tags = [
      "${var.name}",
      "${var.environment}",
      "${lookup(var.node_pools[count.index], "name")}",
    ]

    metadata {
      disable-legacy-endpoints = "true"
    }
  }
}
