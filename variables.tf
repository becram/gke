variable "authorized_master_access" {
  type        = "string"
  description = "External networks that can access the Kubernetes cluster master"
  default     = "0.0.0.0/0"
}

variable "disable_network_policy" {
  type        = "string"
  description = "Determines whether network policy addon is disabled; changing this is a lengthy operation (~10min), and will take roughly another 5 minutes before changes are completed with calico.  Luckily this does not force a cluster rebuild."
  default     = "true"
}

variable "node_network_policy" {
  type        = "string"
  description = "Determines whether network policy for nodes is enabled.  Upon disabling the Network Policy Addon, this **MUST** be changed to `false` first."
  default     = "false"
}

variable "dns_zone_name" {
  type        = "string"
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "environment" {
  type        = "string"
  description = "The environment name"
}

variable "network_policy_provider" {
  type = "string"

  // CALICO is currently the only supported provider
  default = "CALICO"
}

variable "ip_cidr_range" {
  type        = "string"
  description = "The IP range for the cluster"
}

variable "pod_ip_cidr_range" {
  type        = "string"
  description = "Secondary IP range for pods"
  default     = "10.12.0.0/14"
}

variable "service_ip_cidr_range" {
  type        = "string"
  description = "Secondary IP range for services"
  default     = "10.16.0.0/14"
}

variable "kubernetes_version" {
  type        = "string"
  description = "The Kubernetes version. Defaults to latest available"
  default     = ""
}

variable "oauth_scopes" {
  type = "list"

  default = [
    "https://www.googleapis.com/auth/compute",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring",
  ]

  description = "The set of Google API scopes to be made available on all of the node VMs"
}

variable "name" {
  type        = "string"
  description = "The GKE cluster name"
}

variable "private_cluster" {
  type        = "string"
  description = "Whether or not a cluster should be private or not"
  default     = "false"
}

variable "private_master_cidr" {
  type        = "string"
  description = "Master IP range is a private RFC 1918 range for the master's VPC. The master range must not overlap with any subnet in your cluster's VPC. The master and your cluster use VPC peering to communicate privately."
  default     = "172.16.0.0/28"
}

variable "project" {
  type        = "string"
  description = "The project name"
}

variable "region" {
  type        = "string"
  description = "The target region"
}

variable "regional_cluster" {
  type        = "string"
  description = "Creates a cluster with multiple masters spread across zones in the region; forces new resource when changed"
  default     = "true"
}

variable "vpc" {
  type        = "string"
  description = "The target network"
}

variable "enable_flow_logs" {
  type    = "string"
  default = "false"
}

# name - Provide a name for the node pool to be created, must be unique
# machine_type - The type of VM for the worker nodes
# max_node_count - The maximum number nodes per zone
# node_auto_repair - Whether the nodes will be automatically repaired
# node_auto_upgrade - Whether the nodes will be automatically upgraded
# node_disk_size - Size of the disk attached to each node, specified in GB. Forces new resource.
# node_disk_type - Type of the disk attached to each node. Forces new resource.
# preemptible - Use preemptible instances for this node pool
variable "node_pools" {
  default = [
    {
      name              = "node-pool-0"
      machine_type      = "custom-1-1024"
      max_node_count    = "2"
      node_auto_repair  = "true"
      node_auto_upgrade = "true"
      node_disk_size_gb = "10"
      node_disk_type    = "pd-standard"
      preemptible       = "false"
    },
  ]

  description = "Array hosting various configurations for node pools"
}

###############################################
# IAP netblock to allow TCP forwarding
# for ssh access, see https://cloud.google.com/iap/docs/using-tcp-forwarding
variable "enable_cloud_iap_access" {
  type        = "string"
  description = "Determines whether to create a firewall rule for TCP forwarding from IAP"
  default     = "true"
}

variable "cloud_iap_netblock" {
  type = "string"

  default = "35.235.240.0/20"
}

###############################################

