output "cluster_endpoint" {
  value = "${google_container_cluster.cluster.endpoint}"
}

output "gateway_address" {
  value       = "${google_compute_subnetwork.gke.*.gateway_address}"
  description = "The IP address of the gateway."
}

output "subnetwork_self_link" {
  value       = "${google_compute_subnetwork.gke.self_link}"
  description = "The URL of the created subnetwork"
}
