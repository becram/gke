resource "google_compute_subnetwork" "gke" {
  enable_flow_logs         = "${var.enable_flow_logs}"
  name                     = "${format("%v-%v", var.name, var.environment)}"
  network                  = "${var.vpc}"
  project                  = "${var.project}"
  region                   = "${var.region}"
  ip_cidr_range            = "${var.ip_cidr_range}"
  private_ip_google_access = "true"

  secondary_ip_range {
    range_name    = "pod-range-1"
    ip_cidr_range = "${var.pod_ip_cidr_range}"
  }

  secondary_ip_range {
    range_name    = "service-range-1"
    ip_cidr_range = "${var.service_ip_cidr_range}"
  }
}
