resource "google_compute_firewall" "gke-ssh-access" {
  count   = "${var.enable_cloud_iap_access == "true" ? 1 : 0}"
  name    = "${format("%v-%v-ssh-access", var.name, var.environment)}"
  network = "${var.vpc}"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_ranges = ["${var.cloud_iap_netblock}"]

  target_tags = ["${var.name}"]
}
