# Makefile for installing various utilities during CI
# Copyright 2019
# Licence MIT

KEY_SERVERS	 := pool.sks-keyservers.net \
	subkeys.pgp.net \
	pgp.mit.edu \
	keyserver.ubuntu.com \
	keys.gnupg.net
TF_URL		 := https://releases.hashicorp.com/terraform
HASHICORP_KEY	 := 0x51852D87348FFC4C
# These can be overriden, for ex: with either `TF_ARCH=i386 make tfinstall` or `make TF_ARCH=i386 tfinstall`
TF_VERSION	 ?= 0.9.11
TF_DISTRO	 ?= linux
TF_ARCH		 ?= amd64
TF_INSTALL_TO	 ?= /

# These are just for readability
TF_ZIP		 := terraform_$(TF_VERSION)_$(TF_DISTRO)_$(TF_ARCH).zip
TF_SHA256	 := terraform_$(TF_VERSION)_SHA256SUMS
TF_SHA256SIG	 := terraform_$(TF_VERSION)_SHA256SUMS.sig

TF_URL_ZIP	 := $(TF_URL)/$(TF_VERSION)/$(TF_ZIP)
TF_URL_SHA256	 := $(TF_URL)/$(TF_VERSION)/$(TF_SHA256)
TF_URL_SHA256SIG := $(TF_URL)/$(TF_VERSION)/$(TF_SHA256SIG)

.PHONY: gpgkey
gpgkey:		### Get Hashicop's gpg key from list of servers
	@gpg --list-keys $(HASHICORP_KEY); \
	if [ $$? -eq 0 ]; then \
		echo "Key $(HASHICORP_KEY) is already in keystore"; \
	else \
		for ksrv in $(KEY_SERVERS); do \
			echo -n "Getting key $(HASHICORP_KEY) from server $$ksrv ... "; \
			gpg --keyserver $$ksrv --recv-keys $(HASHICORP_KEY); \
			if [ $$? -eq 0 ]; then \
				echo "Success!"; \
				exit 0 ; \
			else \
				echo "Fail"; \
			fi; \
		done; \
	fi

.PHONY: tfinstall
tfinstall:	### Download, check sum and unpack specific terraform version
tfinstall: gpgkey
	@# First, we download into temporary dir
	$(eval $@_TMP := $(shell mktemp -d "/tmp/tfinstall.tmp.XXXXXX"))
	test -n "$($@_TMP)" || exit 1
	wget --quiet --continue --directory-prefix "$($@_TMP)" \
		"$(TF_URL_ZIP)" \
		"$(TF_URL_SHA256)" \
		"$(TF_URL_SHA256SIG)"
	@# Then, we verify signature on hashsums
	gpg --verbose --verify "$($@_TMP)/$(TF_SHA256SIG)" "$($@_TMP)/$(TF_SHA256)"
	@# Then, we verify hashsum on our zip archive, using only its line as stdin
	cd $($@_TMP) && grep "$(TF_ZIP)" "$(TF_SHA256)" | sha256sum -c -w
	@# Finally, we are confident file is legitimate
	unzip -o "$($@_TMP)/$(TF_ZIP)" -d "$(TF_INSTALL_TO)"
	@# Cleanup
	rm -rf "$($@_TMP)"
