# GitLab.com gke Terraform Module

## What is this?

This module is used on the [gitlab-com-infrastructure](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure)
terraform repository for GitLab.com, the staging environment and possibly something else.

This module **REQUIRES** the terraform google provider to be version 2.6.0 or
greater.  Otherwise you'll hit a bug during cluster creation and an apply
operation will not complete.

```
provider "google" {
  version = "2.6.0"
}
```
## Inputs

| Name | Description | Type | Default | Required |
| ---- | ----------- | ---- | ------- | -------- |
| authorized_master_access | External networks that can access the master API | string | 0.0.0.0/0 | no |
| disable_network_policy | Determines whether network policy addon is disabled; changing this is a lengthy operation (~10min), and will take roughly another 5 minutes before changes are completed with calico.  Luckily this does not force a cluster rebuild. | string | true | no |
| node_network_policy | Determines whether network policy for nodes is enabled.  Upon disabling the Network Policy Addon, this **MUST** be changed to `false` first. | string | false | no |
| node_pools | Array hosting various configurations for node pools | [] | See [Node Pool Inputs](./README.md#node-pool-inputs) | no |
| private_cluster | Whether or not a cluster should be private or not | string | false | no |
| private_master_cidr | Master IP range is a private RFC 1918 range for the master's VPC. The master range must not overlap with any subnet in your cluster's VPC. The master and your cluster use VPC peering to communicate privately. | string | 172.16.0.0/28 | no |
| regional_cluster | Creates a cluster with multiple masters spread across zones in the region; forces new resource when changed | string | true | no |

### Node Pool Inputs

| Name | Description | Type | Default | Required |
| ---- | ----------- | ---- | ------- | -------- |
| name | Provide a name for the node pool to be created, must be unique | string | node-pool-0 | yes |
| machine_type | The type of VM for the worker nodes | string | custom-1-1024 | yes |
| max_node_count | The maximum number nodes per zone | string | 2 | yes |
| node_auto_repair | Whether the nodes will be automatically repaired | string | true | yes |
| node_auto_upgrade | Whether the nodes will be automatically upgraded | string | true | yes |
| node_disk_size | Size of the disk attached to each node, specified in GB. Forces new resource. | string | 10 | yes |
| node_disk_type | Type of the disk attached to each node. Forces new resource. | string | pd-standard | yes |
| preemptible | Use preemptible instances for this node pool | string | false | yes |

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written. Reading [Terraform: Up and Running](https://www.amazon.com/Terraform-Running-Writing-Infrastructure-Code-ebook/dp/B06XKHGJHP) is also recommended.

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
